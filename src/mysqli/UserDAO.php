<?php
require_once("User.php");

class UserDAO {
    private $db;

    function __construct(Mysqli $mysqli) {
        $this->db = $mysqli;
    }

    public function find($userId) {
        $sql = "select * from user where id = ?";
        $stmt = $this->db->stmt_init();
        $stmt->prepare($sql);
        
        $stmt->bind_param("i", $userId);
        $stmt->bind_result($id, $name, $email);
        $stmt->execute();
        $stmt->fetch();

        $user = new User();
        $user->setId($id);
        $user->setName($name);
        $user->setEmail($email);

        return $user;
    }

    public function list($order = null) {
        $sql = "select * from user";
        foreach($query_result = $this->db->query($sql) as $userRow) {
            $user = new User();
            $user->setId($userRow["id"]);
            $user->setName($userRow["name"]);
            $user->setEmail($userRow["email"]);

            $users[] = $user;
        }

        return $users;
    }

    public function insert($user) {
        $sql = "insert into user (name, email) values(?, ?)";
        $stmt = $this->db->stmt_init();

        $stmt->prepare($sql);
        $stmt->bind_param("ss", $user->getName(), $user->getEmail());

        $stmt->execute();
        $user->setId($stmt->insert_id);

        return $user;
    }

    public function update($user) {
        $sql = "update user set name = ?, email = ? where id = ?";
        $stmt = $this->db->stmt_init();
        
        $stmt->prepare($sql);
        $stmt->bind_param("ssi", $user->getName(), $user->getEmail(), $user->getId());

        return $stmt->execute();
    }

    public function delete($user) {
        $sql = "delete from user where id = ?";
        $stmt = $this->db->stmt_init();
        $stmt->prepare($sql);

        $stmt->bind_param("i", $user->getId());
        return $stmt->execute();
    }
}
?>