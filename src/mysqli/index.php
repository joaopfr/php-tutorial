<?php
require_once("User.php");
require_once("UserDAO.php");

$dbserver = "172.18.0.2";
$dbuser = "root";
$dbpass = "";
$db = "test";

@$mysqli = new mysqli($dbserver, $dbuser, $dbpass, $db);

if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL($mysqli->connect_errno) $mysqli->connect_error";
    exit;
}

$user = new User();
$user->setId("1");
$user->setName("Ze M");
$user->setEmail("zm@a.com");

$userDAO = new UserDAO($mysqli);
//$user = $userDAO->insert($user);
//echo $user->getId();

//$ret = $userDAO->update($user);
//$ret = $userDAO->delete($user);

//echo "Ret = $ret<br/>";

$users = $userDAO->list();
foreach ($users as $user) {
    echo "Id: {$user->getId()}<br/>";
    echo "Name: {$user->getName()}</br>";
    echo "Email: {$user->getEmail()}</br><hr>";
}

$userFinded = $userDAO->find(3);
echo "Id: {$userFinded->getId()}<br/>";
echo "Name: {$userFinded->getName()}</br>";
echo "Email: {$userFinded->getEmail()}</br><hr>";


$mysqli->close();


?>