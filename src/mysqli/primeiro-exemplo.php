<?php
define("HELLO", "Hello World");
echo HELLO . "\n";

$ar = array("color" => "blue", "value" => 33);

foreach ($ar as $k => $v) {
    echo "$k => $v\n";
}

echo "<br/>";

$dbserver = "172.18.0.2";
$dbuser = "root";
$dbpass = "";
$db = "test";

@$mysqli = new mysqli($dbserver, $dbuser, $dbpass, $db);

if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL:($mysqli->connect_errno) $mysqli->connect_error";
    exit;
}

$sql = "select * from test.user";
$query_result = $mysqli->query($sql);

while ($row = $query_result->fetch_assoc()) {
    echo "Id: $row[id] <br/>";
    echo "Name: $row[name] <br/>";
    echo "Email: $row[email] <br/>";
}

$query_result->close();

echo "<br/>";

foreach ($query_result = $mysqli->query($sql) as $row) {
    echo "Name: $row[name] <br/>";
}

echo "<br/>";

$get_query = "select * from user where id = ?";
$stmt = $mysqli->stmt_init();
$stmt->prepare($get_query);

$stmt->bind_param("i", $_GET["id"]);
$stmt->execute();

$stmt->bind_result($id, $name, $email);
$stmt->fetch();


echo "Id: $id, Name: $name, Email: $email <br/>";
echo "<br/>";

$mysqli->close();
?>