<?php

class Conn implements IConn {

    private $host;
    private $dbname;
    private $user;
    private $pass;

    public function __construct($host, $dbname, $user, $pass) {
        $this->host = $host;
        $this->dbname = $dbname;
        $this->user = $user;
        $this->pass = $pass;
    }

    public function connect() {
        $conn_string = "mysql:host={$this->host};dbname={$this->dbname}";
        try {
            return new \PDO($conn_string, $this->user, $this->pass);
        } catch (\PDOException $e) {
            echo "Erro! Message: {$e->getMessage()}. Code: {$e->getCode()}.";
            exit;
        }
    }
}