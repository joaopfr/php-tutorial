<?php

class ProductDAO {
    private $db;

    public function __construct(IConn $db) {
        $this->db = $db->connect();
    }

    public function find(int $productId) {
        $sql = "select * from product where id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":id", $productId);
        $stmt->execute();

        $productRow = $stmt->fetch(PDO::FETCH_ASSOC);
        $product = new Product();
        $product->setId($productRow["id"])
                ->setName($productRow["name"])
                ->setDescr($productRow["descr"]);
        return $product;
    }

    public function list() {
        $sql = "select * from product";
        foreach ($this->db->query($sql) as $productRow) {
            $product = new Product();
            $product->setId($productRow["id"])
                    ->setName($productRow["name"])
                    ->setDescr($productRow["descr"]);

            $products[] = $product;
        }
        return $products;
    }

    public function save(Product $product) {
        $sql = "insert into product (name, descr) values(:name, :descr)";
        $stmt = $this->db->prepare($sql);

        $stmt->bindValue(":name", $product->getName());
        $stmt->bindValue(":descr", $product->getDescr());

        $stmt->execute();
        return $product->setId($this->db->lastInsertId());
    }

    public function update(Product $product) {
        $sql = "update product set name = :name, descr = :descr where id = :id";
        $stmt = $this->db->prepare($sql);

        $stmt->bindValue(":id", $product->getId());
        $stmt->bindValue(":name", $product->getName());
        $stmt->bindValue(":descr", $product->getDescr());

        $stmt->execute();
        return $product;
    }

    public function delete(int $productId) {
        $sql = "delete from product where id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":id", $productId);

        $ret = $stmt->execute();
        return $ret;
    }
}