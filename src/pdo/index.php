<?php
require_once "IConn.php";
require_once "Conn.php";
require_once "Product.php";
require_once "ProductDAO.php";


$dbserver = "172.18.0.2";
$dbuser = "root";
$dbpass = "";
$db = "test";

$conn = new Conn($dbserver, $db, $dbuser, $dbpass);
// $conn = $conn->connect();

// $sql = "insert into product (name, descr) values(\"eBook\", \"Learn Javascript\")";
// $ret = $conn->exec($sql);

// $sql = "select * from product";
// $stmt =  $conn->query($sql);

// $sql = "select * from product where id = :id";
// $stmt = $conn->prepare($sql);
// $stmt->bindValue(":id", $_GET["id"]);
// $stmt->execute();

// $products = $stmt->fetchAll(PDO::FETCH_ASSOC);
// foreach ($products as $productRow) {
//     echo "Id: {$productRow["id"]}, Name: {$productRow["name"]}, Descr: {$productRow["descr"]}<br/>";
// }

$productDAO = new ProductDAO($conn);

$newProduct = new Product();
$newProduct->setName("CSS Course")->setDescr("Build a stylish website.");

$productDAO->save($newProduct);


$products = $productDAO->list();
foreach ($products as $product) {
    echo "Id: {$product->getId()}, Name: {$product->getName()}, Descr: {$product->getDescr()}<br/>";
}

echo "<hr>";

$newProduct->setDescr("Beautiful websites.");
$productDAO->update($newProduct);

$products = $productDAO->list();
foreach($products as $product) {
    echo "Id: {$product->getId()}, Name: {$product->getName()}, Descr: {$product->getDescr()}<br/>";
}

echo "<hr>";

$product = $productDAO->find($newProduct->getId());
echo "Id: {$product->getId()}, Name: {$product->getName()}, Descr: {$product->getDescr()}<br/>";

echo "<hr>";

echo "{$productDAO->delete($newProduct->getId())} <br/>";
$products = $productDAO->list();
foreach($products as $product) {
    echo "Id: {$product->getId()}, Name: {$product->getName()}, Descr: {$product->getDescr()}<br/>";
}

echo "<hr>";


// foreach ($products as $productObj) {
//     echo "Id: {$productObj->id}, Name: {$productObj->name}, Descr: {$productObj->descr}<br/>";
// }

//   while ($productObj = $stmt->fetch(PDO::FETCH_OBJ)) {
//      echo "Id: {$productObj->id}, Name: {$productObj->name}, Descr: {$productObj->descr}<br/>";        
//   }
 
//    foreach($conn->query($sql) as $productRow) {
//        echo "Id: {$productRow["id"]}, Name: {$productRow["name"]}, Descr: {$productRow["descr"]}<br/>";
//    }

//var_dump($products);

// $stmt->closeCursor();

?>